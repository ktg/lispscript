# The LispScript Programming Language #

(C) 2015 KIM Taegyoon

LispScript is a dialect of Lisp. It is designed to be an embedded language. You can use JavaScript in your LispScript program.

LispScript compiles LispScript code to JavaScript and evaluates it.

* lispscript.js is required to run generated JavaScript code if LispScript's built-in function is used.
* Symbol (variable) names should be valid JavaScript variable names.

Try LispScript here: [LispScript Compiler](http://ktg.bitbucket.org/lispscript/lispscript.html)

Try JavaScript here: [Try JavaScript](http://jsutil.sourceforge.net/tryjs.html)

## Usage (Try LispScript in a web browser) ##
Open lispscript.html.

## Usage (REPL in Microsoft Windows Script Host) ##
Run lispscriptw.bat.

## Usage (REPL in Node.js) ##
Run lispscript.bat or lispscript.sh.

## Usage (in HTML) ##
Using script tag support: Once you include lispscript.js, any script tags in your document with type="text/lispscript" will be evaluated.

```
<script src="lispscript.js"></script>
<script type="text/lispscript">
(alert "hello")
</script>
```

You can also evaluate LispScript code from JavaScript using lispscript.eval(str).
```
<script type="text/javascript">
lispscript.compile(str); // return LispScript program compiled to JavaScript
lispscript.eval(str); // evaluate LispScript program
</script>
```

## Reference ##
```
Functions and macros:
 != !== % * + ++ - -- .
 and at / < << <= = == === > >> >= ^ += -= *= /=
 apply def defmacro defn do eval filter fn for foreach if if/s instanceof js
 label list map new not obj or pr prn read-string return try when when/s while
Etc.:
 (list) [list literal] {object literal} "string" ; end-of-line comment
```

## File ##
* lispscript.js: LispScript language library
* LispScript.html: Try LispScript in a web browser

## Frequently Asked Questions ##
* Lisp-1 or Lisp-2? **Lisp-1**
* Scoping rules? **Lexical scoping**
* 1-way or 2-way JavaScript interop? **2-way**
* Lisp support for JavaScript objects? **Yes**
* Support for tail call optimization? **No**
* Reader macros? **No**
* Speed penalty relative to native JavaScript? **No**
* Unique selling point (i.e. why this rather than one of the other Lisp-to-JavaScript dialects)? **Generating readable JavaScript which is hackable. No speed penalty**

## Examples ##
### Hello, World! ###
```
(prn "Hello, World!")
```

### Comment ###
```
; comment
```

### Function ###

In a function, [lexical scoping](http://en.wikipedia.org/wiki/Lexical_scoping#Lexical_scoping) is used.

```
> (def a 1 b 2) (list a b)
1,2
> ((fn (x y) (+ x y)) 1 2)
3
> ((fn (x) (* x 2)) 3)
6
> (def sum (fn (x y) (+ x y))) ; (def a b) => var a = b;
undefined
> (sum 1 2)
3
> (fold + [1 2 3])
6
> (defn evenp (x) (== 0 (% x 2)))
undefined
> (evenp 3)
false
> (evenp 4)
true
> (apply + [1 2 3])
6
> (map Math.sqrt (list 1 2 3 4))
[ 1, 1.4142135623730951, 1.7320508075688772, 2 ]
> (filter evenp (list 1 2 3 4 5))
[ 2, 4 ]
> (== "abc" "abc")
true
> (= x 1) ; (= a b) => a = b; // global variable
  ((fn (x) (prn x) (= x 3) (prn x)) 4) ; lexical scoping
  x
4
3
1
> (= adder (fn (amount) (fn (x) (+ x amount)))) ; lexical scoping
  (= add3 (adder 3))
  (add3 4)
7
> ; (try TRY-EXPR CATCH-VAR CATCH-EXPR [FINALLY-EXPR ..])
  (try (throw 1) err (prn err) 2 (+ 3 4) 5)
1
5
```

#### Recursion ####
```
> (= factorial (fn (x) (if (<= x 1) x (* x (factorial (- x 1))))))
  (for (def i 1) (<= i 5) (++ i) (prn i (factorial i)))
1 1
2 2
3 6
4 24
5 120
undefined
```

### List ###
```
> (at (list 1 2 3) 1)
4
> (. [1 2 3] length)
3
> (def a [2 4]) (foreach i a (pr i (at a i) ""))
0 2 1 4
```

### Object ###
```
> {"one" 1 "two" 2}
{ one: 1, two: 2 }
> (at (obj "one" 1 "two" 2) "one")
4
> (. {"one" 1 "two" 2} one)
3
> (def a {"one" 1 "two" 2}) (foreach i a (pr i (at a i) ""))
one 1 two 2
```

### Macro ###
```
> (defmacro cos (a) (Math.cos a))
  (defmacro infix (a op ...) (op a ...))
  (cos (infix 0 * 1 2))
1
```

### JavaScript interoperability (from LispScript) ###
LispScript
```
> (alert "Hello") ; call an arbitrary function
> (. Math (random)) ; class's static method
> (Math.random)
> ((. Math random))
0.4780254852371699
> (. Math (floor 1.5))
> (Math.floor 1.5)
1
> (. "abc" length) ; access field
3
> (def a (new Array)) ; or (def a [])
  (. a (push 3))
  (. a (push 4))
  a
3,4
> (js "1+2+3") ; evaluate arbitrary JavaScript code
6
```

### JavaScript interoperability (from JavaScript) ###
JavaScript
```
lispscript.eval("(= a 3)"); // global variable
console.log(a);
```
=> 3

### [99 Bottles of Beer](http://en.wikipedia.org/wiki/99_Bottles_of_Beer) ###
```
(for (def i 99) (>= i 1) (-- i) (prn i "bottles of beer on the wall," i "bottles of beer.")  (prn "Take one down and pass it around," (- i 1) "bottle of beer on the wall."))
```

## License ##

   Copyright 2015 KIM Taegyoon

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

   [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
