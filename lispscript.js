// (C) 2015 KIM Taegyoon
// LispScript language core
// LispScript-to-JavaScript compiler

lispscript = (function() {
  var VERSION="0.1.3";

  // detect browser
  if (typeof(window) === "undefined") {
    if (typeof(WScript) === "undefined") { // node.js?
      var write=function(a){process.stdout.write(String(a));};
      var writeln=console.log;
    }
    else { // Microsoft Windows Script Host
      function write(a) {WScript.StdOut.Write(a)}
      function writeln(a) {WScript.StdOut.WriteLine(a)}
    }
  } else { // in browser
    var write=function(a){document.getElementsByTagName("body")[0].innerHTML+=a;}
    var writeln=function(a){document.getElementsByTagName("body")[0].innerHTML+=a+"<br/>";}

    // <script type="text/lispscript"> support
    function addListener(elem, event, listener) {
      if (elem.addEventListener) elem.addEventListener(event, listener, false);
      else if (elem.attachEvent) elem.attachEvent("on" + event, listener)
    }

    function parseLispTags() {
      var scripts = document.getElementsByTagName("script");
      for (var i = 0; i < scripts.length; ++i) {
        var script = scripts[i];
        if (script.getAttribute("type") == "text/lispscript") {
          eval_string(script.innerHTML);
        }
      }
    }
    addListener(window, "load", parseLispTags)
  }

  function tokenizer(s) {
    var ret = [];
    var acc = ""; // accumulator

    function emit() {
        if (acc.length > 0) {ret.push(acc); acc = "";}
    }

    this.tokenize = function() {
      var last = s.length - 1;
      var unclosed = 0;
      for (var pos=0; pos <= last; pos++) {
          var c = s.charAt(pos);
          if (c == ' ' || c == '\t' || c == '\r' || c == '\n') {
              emit();
          }
          else if (c == ';') { // end-of-line comment
              emit();
              do pos++; while (pos <= last && s.charAt(pos) != '\n');
          }
          else if (c == '"') { // beginning of string
            unclosed++;
            emit();
            acc += c;
            pos++;
            while (pos <= last) {
              c = s.charAt(pos);
              if (c == '"') {unclosed--; acc += c; break;}
              if (c == '\\') { // escape
                var next = s.charAt(pos+1);
                acc += c + next;
                pos += 2;
              } else {
                acc += c;
                pos++;
              }
            }
            emit();
          }
          else if (c == '(' || c == '[' || c == '{') {
            unclosed++;
            emit();
            acc += c;
            emit();
          }
          else if (c == ')' || c == ']' || c == '}') {
            unclosed--;
            emit();
            acc += c;
            emit();
          }
          else {
            acc += c;
          }
      }
      emit();
      if (unclosed != 0) throw new Error("Unclosed: " + unclosed);
      return ret;
    };
  }

  function tokenize(s) {return (new tokenizer(s)).tokenize();}

  function parenthesize(s) {return "(" + s + ")";}

  function parser(tokens) {
    var pos = 0;

    this.parse = function() {
      var tok = tokens[pos];
      if (tok == "(") { // list
        pos++;
        return this.parseList();
      }
      else if (tok == "[") { // list literal
        pos++;
        return this.parseBracket();
      }
      else if (tok == "{") { // object literal
        pos++;
        return this.parseCurly();
      }
      else if (!isNaN(Number(tok))) { // number
        if (tok.charAt(0) == "-") return parenthesize(tok);
        else return tok;
      }
      else { // symbol, etc.
        return tok;
      }
    };

    this.parseList = function() {
      var ret = [];
      var last = tokens.length - 1;
      for (;pos <= last; pos++) {
        var tok = tokens[pos];
        if (tok == ")") { // end of list
          return ret;
        }
        else {
          ret.push(this.parse(tok));
        }
      }
      throw "Unclosed list";
    };

    this.parseBracket = function() {
      var ret = ["list"];
      var last = tokens.length - 1;
      for (;pos <= last; pos++) {
        var tok = tokens[pos];
        if (tok == "]") { // end of list
          return ret;
        }
        else {
          ret.push(this.parse(tok));
        }
      }
      throw "Unclosed list";
    };

    this.parseCurly = function() {
      var ret = ["obj"];
      var last = tokens.length - 1;
      for (;pos <= last; pos++) {
        var tok = tokens[pos];
        if (tok == "}") { // end of list
          return ret;
        }
        else {
          ret.push(this.parse(tok));
        }
      }
      throw "Unclosed list";
    };
  };

  function parse(s) {return (new parser(tokenize(s))).parse();}

  function fn(s) {return parenthesize("function(){"+s+"}");}

  function list() {
    return Array.prototype.slice.call(arguments);
  }

  function obj() {
    var ret = {};
    for (i=0; i<arguments.length; i+=2) {
      ret[arguments[i]] = arguments[i+1];
    }
    return ret;
  }

  function apply(f, lst) {
    return f.apply(null, lst);
  }

  function fold(f, lst) {
    var acc=lst[0];
    for (var i=1; i<lst.length; i++) {
      acc=f(acc,lst[i]);
    }
    return acc;
  }

  function map(f, lst) {
    var ret=[];
    for (var i=0; i<lst.length; i++){
      ret.push(f(lst[i]));}
    return ret;
  }

  function filter(f, lst) {
    var ret=[];
    for (var i=0; i<lst.length; i++){
      var a=f(lst[i]);
      if (a) ret.push(lst[i]);}
    return ret;
  }

  function pr() {
    for (var i=0; i<arguments.length; i++) {
      if(i>0) write(" ");
	  write(arguments[i]);
    }
  }

  function prn() {
	pr.apply(null, arguments);
	writeln("");
  }

  function plus() {
    if (arguments.length == 0) return 0;
    if (arguments.length == 1) return arguments[0];
    var ret=arguments[0];
    for (var i=1; i<arguments.length; i++) {
      ret += arguments[i];
    }
	return ret;
  }

  function minus() {
    if (arguments.length == 0) return 0;
    if (arguments.length == 1) return -arguments[0];
    var ret=arguments[0];
    for (var i=1; i<arguments.length; i++) {
      ret -= arguments[i];
    }
	return ret;
  }

  function star() {
    if (arguments.length == 0) return 1;
    if (arguments.length == 1) return arguments[0];
    var ret=arguments[0];
    for (var i=1; i<arguments.length; i++) {
      ret *= arguments[i];
    }
	return ret;
  }

  function slash() {
    if (arguments.length == 0) return 1;
    if (arguments.length == 1) return 1 / arguments[0];
    var ret=arguments[0];
    for (var i=1; i<arguments.length; i++) {
      ret /= arguments[i];
    }
	return ret;
  }

  var macros={};

  function apply_macro(body, vars) {
    if (body instanceof Array) {
      var ret=[];
      for (var i=0; i<body.length; i++) {
        var b = body[i];
        if (b == "...") {
          ret = ret.concat(vars[b]);
        } else
          ret.push(apply_macro(b, vars));
      }
      return ret;
    } else {
      if (vars.hasOwnProperty(body) && body in vars) return vars[body]; else return body;
    }
  }

  function macroexpand(n) {
    if (macros.hasOwnProperty(n[0]) && n[0] in macros) { // compile macro
      var macro = macros[n[0]];
      var macrovars = {};
      var argsyms = macro[0];
      for (var i=0; i<argsyms.length; i++) {
        var argsym = argsyms[i];
        if (argsym == "...") {
          var ellipsis = macrovars[argsym] = [];
          for (var i2 = i + 1; i2 < n.length; i2++)
            ellipsis.push(n[i2]);
          break;
        } else {
          macrovars[argsym] = n[i+1];
        }
      }
      return apply_macro(macro[1], macrovars);
    } else return n;
  }

  function compile(n) {
    if(n instanceof Array) { // function (FUNCTION ARGUMENT ..)
      switch(n[0]) {
      case "++": // (++ X)
      case "--": // (-- X)
        return parenthesize(n[0] + compile(n[1]));
      case "not": // (not X)
        return parenthesize("!" + compile(n[1]));
      case "%": // (% DIVIDEND DIVISOR)
      case "==":
      case "!=":
      case "!==":
      case "===":
      case "!==":
      case "<":
      case ">":
      case "<=":
      case ">=":
      case "+=":
      case "-=":
      case "*=":
      case "/=":
      case "instanceof":
        return parenthesize(compile(n[1]) + n[0] + compile(n[2]));
      case "+": // (+ X ..)
		if (n.length == 1) return "0";
		if (n.length == 2) return compile(n[1]);
        var ret=compile(n[1]);
        for (var i=2; i<n.length; i++) {
          ret += n[0] + compile(n[i]);
        }
        return parenthesize(ret);
      case "-":
		if (n.length == 1) return "0";
		if (n.length == 2) return parenthesize("-" + compile(n[1]));
        var ret=compile(n[1]);
        for (var i=2; i<n.length; i++) {
          ret += n[0] + compile(n[i]);
        }
        return parenthesize(ret);
      case "*":
		if (n.length == 1) return "1";
		if (n.length == 2) return compile(n[1]);
        var ret=compile(n[1]);
        for (var i=2; i<n.length; i++) {
          ret += n[0] + compile(n[i]);
        }
        return parenthesize(ret);
      case "/":
		if (n.length == 1) return "1";
		if (n.length == 2) return parenthesize("1 / " + compile(n[1]));
        var ret=compile(n[1]);
        for (var i=2; i<n.length; i++) {
          ret += n[0] + compile(n[i]);
        }
        return parenthesize(ret);
      case "=":
      case "<<":
      case ">>":
        var ret=compile(n[1]);
        for (var i=2; i<n.length; i++) {
          ret += n[0] + compile(n[i]);
        }
        return parenthesize(ret);
      case "and":
        var ret=compile(n[1]);
        for (var i=2; i<n.length; i++) {
          ret += "&&" + compile(n[i]);
        }
        return parenthesize(ret);
      case "or":
        var ret=compile(n[1]);
        for (var i=2; i<n.length; i++) {
          ret += "||" + compile(n[i]);
        }
        return parenthesize(ret);
      case "def": // (def SYMBOL VALUE ..)
        var ret = "var ";
        for (var i = 1; i < n.length; i += 2) {
          if (i > 1) ret += ", ";
          ret += n[i] + " = " + compile(n[i + 1]);
        }
        return ret;
      case "if": // (if CONDITION THEN_EXPR ELSE_EXPR)
        return parenthesize(compile(n[1]) + "?" + compile(n[2]) + ":" + compile(n[3]));
      case "if/s": // (if CONDITION THEN_EXPR ELSE_EXPR) ; if statement
        var ret = "if("+compile(n[1]) + ")" + compile(n[2]) + ";";
        if (n.length >= 4) ret += "else " + compile(n[3]) + ";";
        return ret;
      case "for": // (for INIT COND STEP EXPR ..) ; for statement
        var len = n.length;
        var init = compile(n[1]);
        var cond = compile(n[2]);
        var step = compile(n[3]);
        var ret = "for("+init+"; "+cond+"; "+step+"){\n";
        for (var i=4; i<len; i++) {
          ret += compile(n[i])+";\n";
        }
        ret += "}";
        return ret;
      case "foreach": // (for KEY CONTAINER EXPR ..) ; for in statement
        var len = n.length;
        var key = compile(n[1]);
        var container = compile(n[2]);
        var ret = "for(var "+key+" in "+container+"){if ("+container+".hasOwnProperty("+key+")) {\n";
        for (var i=3; i<len; i++) {
          ret += compile(n[i])+";\n";
        }
        ret += "}}";
        return ret;
      case "while": // (while CONDITION EXPR ..)
        var cond = compile(n[1]);
        var len = n.length;
        var ret = "while(" + cond + "){\n";
        for (var i = 2; i < len; i++) {
            ret+=compile(n[i])+";\n";
        }
        ret+="}";
        return ret;
      case "break": // (break [LABEL])
      case "continue":
        if (n.length >= 2) return n[0] + " " + n[1];
        else return n[0];
      case "label": // (label LABEL BODY)
        return n[1] + ":" + compile(n[2]);
      case "return": // (return ARGUMENT)
        return n[0] + " " + compile(n[1]);
      case "fn": // (fn (ARGUMENT ..) BODY ..) => lexical closure
        var ret = "function(";
        var args = n[1];
        ret += args.join(",")+"){\n";
        for(var i=2;i<n.length-1;i++) {
          ret+=compile(n[i])+";\n";
        }
        ret+="return " + compile(n[n.length-1])+";";
        ret+="}";
        return parenthesize(ret);
      case "do": // (do X ..)
        var last = n.length - 1;
        var ret="(";
        for (var i = 1; i <= last; i++) {
            ret+=compile(n[i]);
            if (i < last) ret+=", ";
        }
        return ret + ")";
      case "do/s": // (do/s X ..) ; statement
        var last = n.length - 1;
        var ret="{";
        for (var i = 1; i <= last; i++) {
            ret+=compile(n[i]) + "; ";
        }
        return ret + "}";
      case "new":
          // (new CLASS ARG ..) ; create new object
          // => new CLASS(ARG,..)
          var ret = "new " + n[1] + "(";
          var n2 = n.slice(2);
          for(var i=0;i<n2.length;i++) {
            n2[i]=compile(n2[i]);
          }
          ret+=n2.join(",")+")";
          return parenthesize(ret);
      case "list":
        var ret = "[";
        var n2 = n.slice(1);
        for(var i=0;i<n2.length;i++) {
          if (i > 0) ret += ",";
          ret += compile(n2[i]);
        }
        ret += "]";
        return ret;
      case "at": // ([] "abc" "length") ; access field
        return compile(n[1])+"["+compile(n[2])+"]";
      case ".": // (. "abc" length ...) (. Math (floor 1.5)); access field or method, chainable
        var ret=compile(n[1]);
        for (var i=2; i<n.length; i++) {
          ret += "." + compile(n[i]);
        }
        return parenthesize(ret);
      case "try": // (try TRY-EXPR CATCH-VAR CATCH-EXPR [FINALLY-EXPR ..])
		var ret = "try {" + compile(n[1]) + "}\ncatch (" + compile(n[2]) + ") {\n" + compile(n[3]) + "}";
		if (n.length >= 5) {
			ret += "\nfinally {";
			var n2 = n.slice(4);
			for(var i=0;i<n2.length;i++) {
				n2[i]=compile(n2[i]);
			}
			ret += n2.join(";")+"}";
		}
		return ret;
      case "defmacro": // (defmacro add (a b) (+ a b)) ; define macro
        macros[n[1]] = [n[2], n[3]];
        return "";
      default: // (func ARGUMENT ..) ; general function
        if (macros.hasOwnProperty(n[0]) && n[0] in macros) { // compile macro
          return compile(macroexpand(n));
        } else {
          var ret = compile(n[0])+"(";
          var n2 = n.slice(1);
          for(var i=0;i<n2.length;i++) {
            n2[i]=compile(n2[i]);
          }
          ret+=n2.join(",")+")";
          return ret;
        }
      } // end switch
    }
    else {
      switch(n) { // compile built-in function as first-class object
      case "+": // (+ X ..)
		return "lispscript.plus";
      case "-":
		return "lispscript.minus";
      case "*":
		return "lispscript.star";
      case "/":
		return "lispscript.slash";
      case "%": // (% DIVIDEND DIVISOR)
      case "==":
      case "!=":
      case "===":
      case "!==":
      case "<":
      case ">":
      case "<=":
      case ">=":
          return fn('return arguments[0]'+n+'arguments[1];');
      case "and":
        return fn('for(var _i=0;_i<arguments.length;_i++){if(!arguments[_i])return false;} return true;');
      case "or":
        return fn('for(var _i=0;_i<arguments.length;_i++){if(arguments[_i])return true;} return false;');
      case "not": // (not X)
          return fn('return !arguments[0];');
      case "read-string": // (read-string X)
          return "lispscript.parse";
      case "eval": // (eval X)
          return "lispscript.eval_sexp";
      case "list": // (list X ..)
      case "obj": // (obj X ..)
      case "apply": // (apply FUNC LIST)
      case "fold": // (fold FUNC LIST)
      case "map": // (map FUNC LIST)
      case "filter": // (filter FUNC LIST)
      case "pr": // (pr X ..)
      case "prn": // (prn X ..)
          return "lispscript." + n;
      case "js": // (js "JS CODE")
          return "eval";
      default:
          return n;
      }
    }
  }

  function compile_all(exprs) {
    ret = "";
    for (var i=0; i<exprs.length; i++) {
      ret += compile(exprs[i]) + ";\n";
    }
    return ret;
  }

  function compile_string(s) {
    return compile_all(parse("(" + s + "\n)"));
  }

  function eval_string(s) { // eval on global scope
    return eval.apply(null, [compile_string(s)]);
  }

  eval_string('(defmacro defn (name ...) (def name (fn ...)))');
  eval_string('(defmacro when (cond ...) (if cond (do ...)))');
  eval_string('(defmacro when/s (cond ...) (if/s cond (do/s ...)))');

  return {VERSION:VERSION, eval:eval_string, compile:compile_string, tokenize:tokenize, parse:parse, pr:pr, prn:prn, list:list, obj:obj, apply:apply, fold:fold, map:map, filter:filter, plus:plus, minus:minus, star:star, slash:slash};
})();

if (typeof(window) === "undefined") {
  // start REPL
  lispscript.prn('LispScript ' + lispscript.VERSION);
  if (typeof(WScript) === "undefined") { // node.js?
    var readline = require('readline');
    var rl = readline.createInterface(process.stdin, process.stdout);
    rl.setPrompt('> ');
    rl.on('line', function(line) {
      try{
				// pretty print
				var result = lispscript.eval(line);
				if (typeof(result) == "function")
					lispscript.prn(result); // print function definition
				else
					console.log(result);
      }catch (err) {
        lispscript.prn(err);
      }
      rl.prompt();
    });
    rl.prompt();
  }
  else { // Microsoft Windows Script Host?
    while (true) {
      try{
        lispscript.pr("> ");
        if (WScript.StdIn.AtEndOfStream) break;
        var line = WScript.StdIn.ReadLine();
        var code = lispscript.compile(line);
        lispscript.prn(eval(code));
      }
      catch(err) {
        lispscript.prn(err.message);
      }
    }
  }
}
